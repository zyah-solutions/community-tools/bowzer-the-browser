## Warning
Backup "vi.lib/ActorFramework" and "resource/AFDebug" before attempting.

## Instructions

1) Follow the instructions in https://forums.ni.com/t5/Actor-Framework-Discussions/How-to-build-a-PPL-including-Actor-classes-messages-and-AF-Debug/m-p/3871947/highlight/true#M5945 through step 14 with the following additions:
    * Add "AF_Debug_Trace = TRUE" to your 'Condidtional Disable Symbols' in your project properties and save everything.
2) Add a new library to the project called "TEMP.lvlib" and save it in "LabVIEW 20xx\"
3) Add "Actor Framework.lvlib" to "TEMP.lvlib" and save all.
4) Use "Save as..." on "TEMP.lvlib" to create a new copy of the library and its contents in a new location on disk (e.g. "New AF.lvlib").
    * Along with the new library, "vi.lib" and "resource" directories will be created.
5) Remove TEMP.lvlib from your project, save it and close LabVIEW.
6) Reopen LabVIEW and the project and add "New AF.lvlib" to it (if not added during step 4).
7) Rename "Actor Framework.lvlib" to (e.g.) "Alternate Actor Framework.lvlib" and save all.
8) Remove "Alternate Actor Framework.lvlib" from the "New AF.lvlib" library and save all.
9) Delete "New AF.lvlib" from disk.
    * Do NOT restore your backup folders yet.
10) Your "Alternate Actor Framework.lvlib" file can now replace "Actor Framework.lvlib" in your projects.
11) Only now, restore the "vi.lib/ActorFramework" and "resource/AFDebug" folders from your backup and delete "LabVIEW 20xx\TEMP.lvlib".

**Final note:** Any VIs that were not in the original "Actor Framework" library (e.g. "Time-Delayed Send Message") will revert back to their original versions after step 11 - you must manually replace these VIs (if you used them in your project) with the new "Alternate Actor Framework.lvlib" versions.
