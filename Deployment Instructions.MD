# Deployment Instructions
In order for Bowzer the Browser (which uses AF) to not lock other AF libraries when browsing, we use a duplicate copy of the AF libraries.

## Commit code
1) Commit and push any code changes before starting the rest of the deployment procedure.
2) Create a new branch specific to the version being released.

## Replacing the standard AF libraries
1) Find "Actor Framework.lvlib" in the project dependencies, right click and choose "Replace with...".
2) Choose "\Source\Bowzer the Browser\_Alternate AF\vi.lib\ActorFramework\Alternate Actor Framework.lvlib" as the replacement library.
3) If/when prompted, save any and all VIs.
4) Save all in the project and close.

## Other steps
* Commit and push the code in the release branch associated with the specific version.
	* Tag the branch with the version info.
* Switch back to the develop branch (i.e. back to the native Actor Framework library) before continuing any further development while keeping the updated vipb file (new build number).
* Commit the updated vipb file so that future release builds will increment correctly.